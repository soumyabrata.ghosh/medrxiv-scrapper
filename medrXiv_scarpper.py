#!/usr/bin/env python
# coding: utf-8

# In[ ]:


'''
Extract abstracts from all latest COVID-19 papers on BiorXiv and MedrXiv
'''

import json
import re
import urllib.request
import os
import datetime
import time
import os, json
import pandas as pd
import json
import sys

#urllib.request.urlretrieve('https://connect.medrxiv.org/relate/collection_json.php?grp=181', 'medRxiv.json')

# returns a dict of papers, key is DOI
def get_papers_from_biorxiv_json(jsonfile):
    with open(jsonfile) as w:
        contents = w.readlines()

    contents = json.loads(contents[0])['rels']
    contents = contents[:-1] # remove empty last value, depends on BiorXiv json formatting
    papers = dict()
    for paper in contents:
        papers[paper['rel_doi']] = paper
    return papers

# get the biorXiv/medrXiv html for a DOI
def get_htmlstr(doi):
    fp = urllib.request.urlopen('http://doi.org/' + doi)
    mybytes = fp.read()
    mystr = mybytes.decode("utf8")
    fp.close()
    return mystr


def printProgressBar(i,max,postText):
    n_bar =100 #size of progress bar
    j= i/max
    sys.stdout.write('\r')
    sys.stdout.write(f"[{'=' * int(n_bar * j):{n_bar}s}] {int(100 * j)}%  {postText}")
    sys.stdout.flush()

# Extract abstract text from HTML of webpage
def get_abstract(htmlstr):
    try:
        # observation: most scientific information seems to be captured in first abstract paragraph
        # competing interests/disclosures/author info can affect clustering, removed here
        abstractstr = htmlstr[htmlstr.rindex('Abstract</h2>')+13:]
        toremove = abstractstr[abstractstr.index('</p>'):]
        abstractstr = abstractstr[:-len(toremove)]
        #abstractl = re.findall(pattern, str(abstract))
    except:
        # manually added in some patterns for specific cases
        #try:
        #pattern = 'Abstract</h2>(.*?)\</p><div'
        #abstractstr = re.search(pattern, str(htmlstr)).group(1)
        #except:
        #try:
        #pattern = 'Abstract</h2>(.*?)\</p></div'
        #abstractstr = re.search(pattern, str(htmlstr)).group(1)
        #except:
        try:
            pattern = 'content="(.*?)\"'
            abstractl = re.findall(pattern, str(htmlstr))
            abstractstr = max(abstractl, key=len)
        except:
            print("Error in extracting abstract from HTML string")
            return ''
    abstractstr = abstractstr.replace('\n','') # remove newline 
    return re.sub('<[^>]+>', '', abstractstr) # remove other formatting elements

def main():
    jsons_data = pd.DataFrame(columns=['date', 'pmid', 'title', 'journal', 'abstract', 'topics', 'keywords', 'doi'])

    # download latest BiorXiv/MedrXiv json dump
    os.system('curl -O https://connect.biorxiv.org/relate/collection_json.php?grp=181')
    ts = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d')
    jsonfilename = 'medrXiv_covid19_import.json'
    os.system('mv collection_json.php?grp=181 {0}'.format(jsonfilename))
    
    # get DOI indexed dict of papers
    papers = get_papers_from_biorxiv_json(jsonfilename)
    #print(papers['10.1101/2020.03.24.20042291'])
    # get abstracts for each paper
    paper_count = 0
    total_papers = len(papers.keys())
    print("Total papers: "+str(total_papers))
    #toolbar_width = 100 #len(papers.keys())
    # setup toolbar
    #bar = Bar('Loading', fill='@', suffix='%(percent)d%%')
    #sys.stdout.write("[%s]" % (" " * toolbar_width))
    #sys.stdout.flush()
    #sys.stdout.write("\b" * (toolbar_width+1)) # return to start of line, after '['
    #oldratio = 0
    for doi in papers.keys():
        try:
            htmlstr = get_htmlstr(doi)
            abstract = get_abstract(htmlstr)
            # check if at least 10% of the words > 3 characters long in the title are present in the abstract - to avoid picking up names/other text. This approximation sometimes doesn't work great for abstracts that are well scraped
            # titlel = [x for x in papers[doi]['rel_title'].split(' ') if len(x) > 3]
            #if len([x for x in titlel if x in abstract]) < np.ceil(0.1 * len(titlel)):
            #print(doi + ', ' + papers[doi]['rel_title'] + ': WRONG abstract text scraped')#papers[doi]['rel_abstract'] = ''
            #continue
            papers[doi]['rel_abstract'] = abstract
            #print(doi + ', ' + papers[doi]['rel_title'] + ','+ papers[doi]['rel_authors'] +','+abstract+',SUCCESS')
            #print(abstract)
        except:
            #print(doi + ', ' + papers[doi]['rel_title'] + ','+ papers[doi]['rel_authors'] +',,FAILED abstract scraping')
            papers[doi]['rel_abstract'] = ''
        # write papers + abstracts to json file
    #with open('bio_medrXiv_covid19_' + ts + '_abstracts.json', 'w', encoding='utf-8') as f:
    #    json.dump(papers, f, ensure_ascii=False, indent=4)
    #    if paper_count/total_papers*100 > oldratio + 1:
    #        sys.stdout.write("-")
    #        sys.stdout.flush()
    #        oldratio = paper_count/total_papers*100
       # if paper_count == 20:
        #    break
        printProgressBar(paper_count,total_papers,"processed") 
        paper_count = paper_count +1
        #bar.next()
    #bar.finish()
    #sys.stdout.write("]\n") # this ends the progress bar
    #print(paper_count)
    #print(json.dumps(papers, indent=4))
    with open('medrXiv_covid_export_' + str(ts) + '.json', 'w', encoding='utf-8') as f:
        json.dump(papers, f, ensure_ascii=False, indent=4)

    #print(papers)
        
if __name__ == '__main__':
    main()


# In[ ]:




